## Update Service

Connects and monitors ActiveMQ messages from back end resources

### Message Types

The following message types specific to Rabbies booking engine are monitored:

- Tour : Action 'Update' : number value changes
    - unpaid
    - reserved
    - available
    - confirmed
    
- Tour : Action 'Update' : string value changes
    - driver
    - tour status
        
- Booking : Action 'Add' 
- Booking : Action 'Delete'
       
- Task : Action 'Add'
- Task : Action 'Delete'
- Task : Action 'Complete'
       
### Message Formats
       
This service shall emit messages in the following format via web socket connections:
       
- message-type : string
- message-object :  object
       
EG Here is the full list of message types and typical objects:
       
- 'tour-update-unpaid' : {tour: id, unpaid: 0}       
- 'tour-update-reserved' : {tour: id, reserved: 0}       
- 'tour-update-available' : {tour: id, available: 0}       
- 'tour-update-confirmed' : {tour: id, confirmed: 0}       
- 'tour-update-driver' : {tour: id, driver: 'fred bloggs'}       
- 'tour-update-status' : {tour: id, status: 'Full'}       
 
- 'booking-add' : {booking: Booking}
- 'booking-delete' : {booking: id}
       
- 'task-add' : {task: Task}
- 'task-delete' : {task: id};
- 'task-complete' : {task: id}
       
       
    
    