"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by duncmcm on 29/07/2017.
 */
var express = require("express");
var bodyParser = require("body-parser");
var Stomp = require('stompjs');
/** Create the express server */
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
function sendTestMessages() {
    var reserved = 0;
    setInterval(function () {
        var messageObject = { tour: 123456, reserved: reserved };
        io.emit('tour-update-reserved', messageObject);
        reserved++;
    }, 1000);
}
function sendActiveMQMessage(message) {
    io.emit('task_ui_updates', message);
}
/** Listen on the express server */
var server = http.listen(2360, function () {
    var port = server.address().port;
    console.log('HTTP Update Service active on Port', port);
    sendTestMessages();
});
io.on('connection', function (socket) {
    console.log('a client connected with socket ' + socket);
    socket.on('disconnect', function () {
        console.log('user disconnected');
    });
});
/** Using stompjs **/
var destination = '/queue/task_ui_updates';
var subscription;
function receivedMessage(message) {
    // called when the client receives a STOMP message from the server
    if (message.body) {
        console.log('ActiveMQ got message with body ' + message.body);
    }
    else {
        console.log('ActiveMQ got empty message');
    }
    sendActiveMQMessage(message);
}
var client = Stomp.overWS('ws://localhost:61614/stomp');
client.connect('admin', 'admin', stompConnect, stompError);
client.heartbeat.outgoing = 20000; // client will send heartbeats every 20000ms
client.heartbeat.incoming = 0; // client does not want to receive heartbeats
// from the server
function stompConnect() {
    console.log('ActiveMQ stomp node client connected');
    client.send(destination, {}, 'Hello from Rab-Demo ActiveMQ node client');
    subscription = client.subscribe('/queue/task_ui_updates', receivedMessage, { id: 2778 });
}
function stompError() {
    console.log('ActiveMQ stomp node client error');
}
function disconnect() {
    client.disconnect(function () {
        console.log('ActiveMQ See you next time!');
    });
    subscription.unsubscribe();
}
exports = module.exports = app;
//# sourceMappingURL=update-service.js.map
