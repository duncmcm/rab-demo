/**
 * Created by duncmcm on 17/03/2018.
 */
let chokidar = require('chokidar');
let _ = require('underscore');
let fs = require('fs');
const util = require('util');

import express = require('express');

let debug = true;

/** Create the express server */
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

let pathToWatch = './config/';
let watcher = chokidar.watch(pathToWatch, {awaitWriteFinish: true, ignoreInitial: true,
  ignored: [('**/*.tmp'), '*/.DS_Store'], persistent: true});

watcher.on('change', function(path: any) {
    if (debug) {
        let currentdate = new Date();
        let datetime = 'Date: ' + currentdate.getDay() + '/' + currentdate.getMonth() + '/' + currentdate.getFullYear()
          + ' @ ' + currentdate.getHours() + ':' + currentdate.getMinutes() + ':' + currentdate.getSeconds();
        console.log(datetime + ' Changed: ' + path);
    }
    parseFile(path);
});

watcher.on('add', function(path: any) {
    if (debug) {
        let currentdate = new Date();
        let datetime = 'Date: ' + currentdate.getDay() + '/' + currentdate.getMonth() + '/' + currentdate.getFullYear()
          + ' @ ' + currentdate.getHours() + ':' + currentdate.getMinutes() + ':' + currentdate.getSeconds();
        console.log(datetime + ' Added: ' + path);
    }
    parseFile(path);
});

function parseFile(path: any) {
    fs.readFile(path, 'utf8', function(err: any, data: any) {
        /** let dataString = JSON.stringify(data); **/
        let fileObj = JSON.parse(data);
        console.log('File Data ' + fileObj.config.name);

        sendConfigChangeMessage(fileObj);
    });
}

function sendConfigChangeMessage(data: any) {
    let configName = data.config.name;
    let messageType = configName + '-config';
    io.emit(messageType, data);
}

/** Listen on the express server */
const server = http.listen(2370, () => {
    const port = server.address().port;
    console.log('Config File Change Service active on Port', port);
});

io.on('connection', function(socket: any){
    console.log('a client connected with socket ' + socket);
    socket.on('disconnect', function(){
        console.log('user disconnected');
    });
});

exports = module.exports = app;
