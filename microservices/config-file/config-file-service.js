"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by duncmcm on 17/03/2018.
 */
var chokidar = require('chokidar');
var _ = require("underscore");
var fs = require('fs');
var util = require('util');
var express = require("express");
var debug = true;
/** Create the express server */
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var pathToWatch = './config/';
var watcher = chokidar.watch(pathToWatch, { awaitWriteFinish: true, ignoreInitial: true, ignored: [('**/*.tmp'), '*/.DS_Store'], persistent: true });
watcher.on('change', function (path) {
    if (debug) {
        var currentdate = new Date();
        var datetime = "Date: " + currentdate.getDay() + "/" + currentdate.getMonth() + "/" + currentdate.getFullYear() + " @ " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
        console.log(datetime + ' Changed: ' + path);
    }
    parseFile(path);
});
watcher.on('add', function (path) {
    if (debug) {
        var currentdate = new Date();
        var datetime = "Date: " + currentdate.getDay() + "/" + currentdate.getMonth() + "/" + currentdate.getFullYear() + " @ " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
        console.log(datetime + ' Added: ' + path);
    }
    parseFile(path);
});
function parseFile(path) {
    fs.readFile(path, "utf8", function (err, data) {
        //let dataString = JSON.stringify(data);
        var fileObj = JSON.parse(data);
        console.log('File Data ' + fileObj.config.name);
        sendConfigChangeMessage(fileObj);
    });
}
function sendConfigChangeMessage(data) {
    var configName = data.config.name;
    var messageType = configName + '-config';
    io.emit(messageType, data);
}
/** Listen on the express server */
var server = http.listen(2370, function () {
    var port = server.address().port;
    console.log('Config File Change Service active on Port', port);
});
io.on('connection', function (socket) {
    console.log('a client connected with socket ' + socket);
    socket.on('disconnect', function () {
        console.log('user disconnected');
    });
});
exports = module.exports = app;
//# sourceMappingURL=config-file-service.js.map