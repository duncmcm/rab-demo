"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by duncmcm on 17/02/2018.
 */
var express = require("express");
var bodyParser = require("body-parser");
var request = require("request");
/** Create the express server */
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
/** Fast SMS Token **/
var token = '1XBk-6UAd-ZR6g-lWeR';
var mobile = 447481837733;
var APIUrl = 'https://my.fastsms.co.uk/api';
var action = 'Send';
var message = 'Hi From Duncan';
var getCreditsUrl = 'https://my.fastsms.co.uk/api?Token=1XBk-6UAd-ZR6g-lWeR&Action=CheckCredits&ShowErrorMessage=1';
var getMessageStatus = 'https://my.fastsms.co.uk/api?MessageID=235192450&Token=1XBk-6UAd-ZR6g-lWeR&Action=CheckMessageStatus&ShowErrorMessage=1';
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
function sendTestSMS() {
    var fullUrl = addPostVars();
    fullUrl = getMessageStatus; //'https://my.fastsms.co.uk/api?Token=1XBk-6UAd-ZR6g-lWeR&Action=CheckCredits&ShowErrorMessage=1';
    console.log('Full Url: ' + fullUrl);
    request(fullUrl, function (error, response, body) {
        console.log('error:', error); // Print the error if one occurred
        console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
        console.log('body:', body); // Print the result
    });
}
function logError(error) {
}
function parseData(data) {
}
function addPostVars() {
    var fullUrl = APIUrl + '?Token=' + token;
    fullUrl = fullUrl + '&Action=' + action;
    fullUrl = fullUrl + '&DestinationAddress=[' + mobile + ']';
    fullUrl = fullUrl + '&ValidityPeriod=86400' + '&GetAllMessageIDs=1' + '&GetBGSendID=1' + '&Body=Hello%20World';
    return fullUrl;
}
/** Listen on the express server */
var server = http.listen(2359, function () {
    var port = server.address().port;
    console.log('HTTP SMS Service active on Port', port);
    setInterval(function () {
        sendTestSMS();
    }, 10000);
});
io.on('connection', function (socket) {
    console.log('a client connected with socket ' + socket);
    socket.on('disconnect', function () {
        console.log('user disconnected');
    });
});
exports = module.exports = app;
//# sourceMappingURL=sms-service.js.map
