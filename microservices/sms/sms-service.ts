/**
 * Created by duncmcm on 17/02/2018.
 */
import express = require('express');
import bodyParser = require('body-parser');
import request = require('request');

/** Create the express server */
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

/** Fast SMS Token **/
const token = '1XBk-6UAd-ZR6g-lWeR';
const mobile = 447481837733;
const APIUrl = 'https://my.fastsms.co.uk/api';
const action = 'Send';
const message = 'Hi From Duncan';
const getCreditsUrl = 'https://my.fastsms.co.uk/api?Token=1XBk-6UAd-ZR6g-lWeR&Action=CheckCredits&ShowErrorMessage=1';
const getMessageStatus = 'https://my.fastsms.co.uk/api?MessageID=235192450&Token=1XBk-6UAd-ZR6g-lWeR&Action=CheckMessageStatus&ShowErrorMessage=1';
const sendMessage = 'https://my.fastsms.co.uk/api?DestinationAddress=%2B447481837733%2C+%2B447864033524&ListName=&GroupName=&SourceAddress=Rabbies&Body=Welcome+to+FastSMS&ScheduleDate=20180305114541&SourceTON=&ValidityPeriod=1000&GetAllMessageIDs=&GetBGSendID=&Token=1XBk-6UAd-ZR6g-lWeR&Action=Send&ShowErrorMessage=1';

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

function sendTestSMS() {

    let fullUrl = getMessageStatus; //'https://my.fastsms.co.uk/api?Token=1XBk-6UAd-ZR6g-lWeR&Action=CheckCredits&ShowErrorMessage=1';
    console.log('Full Url: ' + fullUrl);

    request(fullUrl, function (error, response, body) {
      console.log('error:', error); // Print the error if one occurred
      console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('body:', body); // Print the result
    });
}

function logError(error: any) {

}

function parseData(data: any) {

}

/** Listen on the express server */
const server = http.listen(2359, () => {
    const port = server.address().port;
    console.log('HTTP SMS Service active on Port', port);

    setInterval( function() {
        sendTestSMS();
    }, 10000);

});

io.on('connection', function(socket: any){
    console.log('a client connected with socket ' + socket);
    socket.on('disconnect', function(){
        console.log('user disconnected');
    });
});

exports = module.exports = app;

