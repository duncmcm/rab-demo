# Step 4 - Create custom Components and Directives

create a Utilities Module to hold reusable Directives and Components
- ng g module ultilities
- cd utilities
- mkdir components directives
- cd directives
- ng g directive halo
- cd ../components
- ng g component task-list
- In the UtilitiesModule 'exports' these new classes, so they can be used anywhere

## Designing the Custom 'Task-List' component

This TaskList custom component has the following requirements:
- Shows, in a vertical layout, a list of Tasks
- Each Task is represented by a Angular Material card component
- By default the task list scrolls through the cards automatically on display
- the user can stop the scroll by a mouse over action (and visa versa)
- The Task list can be removed using <ESC> of a mouse click out event
- Scrolling can be resumed (up|down) by mouseover either Up or Down arrows
- A placement indicator is a sub component which will indicate the central card position
  in summary. IE card 3 central from 12 cards
- The task list data shall be served by a dedicated service  
- The Halo directive shall indicate when a card item is removed (red halo) or added (green halo)
- Clicking a card shall route the user to the action and required route
- Sufficient details within the card shall show task type IE 'tour', task details, owner etc
  
## Designing the HaloDirective
  
This HaloDirective has the following requirements:
  
  