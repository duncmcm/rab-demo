# Step 1 - Generate new project

Run the following commands:
- ng new rab-demo
- cd rab-demo
- npm install
- ng eject
- npm start

## Default Project structure

Mention :
- json files
- webpack.config.js


## First project build - Angular 5 upgrade etc

Mention :
- package.json
- bootstrap AppComponent
- index.html (nothing in it)

### Add some basic styles and my first Angular component

- cd src, cd app
- mkdir components, mkdir modules
- cd components
- ng g component top-nav (component auto added to appModule class)
- default html css files for component
- add component selector to appComponent.html, show result
- add top-nav-component.html and associated top-nav-component.css, show result
- add font-awesome as import to styles.css file
- mention 'src/styles.css' one of the entry points in webpack.config.js

### Add a 3rd party library

- If I hadn't copied my package.json file, to add a library using NPM do:
- npm install @angular/material@latest --save
- library dependencies -> style and icon imports (index.html and styles.css)
- import Component Modules into Angular Module (appModule.ts)
- some animation dependencies IE BrowserAnimationModule
- to override 3rd party styles, cannot put in appComponent.css file as this is specific to that component, need to put into top level styles.css file



