# Step 2 - Design by Modules with Routes

Structuring your app features and generic utilities into modules provides :
- good segregation of your code
- improve your component re-usability
- streamline your build process
- provide app performance improvements IE module lazy loading

## Create your first feature module - TourModule

- cd src, cd app, cd modules
- ng g module tour
- add this new module to your appModule imports
- cd tour
- ng g component tour-summary
- the new component has been added to the TourModule declarations
 
## Add routing to your module


