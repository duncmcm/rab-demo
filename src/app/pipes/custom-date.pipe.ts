import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import {DatePipe} from '@angular/common';

@Pipe({
  name: 'customDate'
})
export class CustomDatePipe extends DatePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (args === 'dbISO') {
		let date = moment(value, 'YYYY-MM-DD').format('DD-MM-YYYY');
		// console.log('DATE :' + date);
		return date;
    } else if (args === 'forDB') {
		let date = moment(value, 'DD-MM-YYYY').format('YYYY-MM-DD');
		// console.log('DATE :' + date);
		return date;
	}
    return null;
  }

}
