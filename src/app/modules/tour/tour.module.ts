import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TourSummaryComponent } from './tour-summary/tour-summary.component';
import {RouterModule, Routes} from '@angular/router';
import {
  MatInputModule,
  MatPaginatorModule,
  MatRippleModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import {SubPanelModule} from '../sub-panel/sub-panel.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

const tourRoutes: Routes = [
  { path: 'tours', component: TourSummaryComponent},
];

@NgModule({
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  imports: [
    CommonModule,
    RouterModule.forChild(tourRoutes),
    BrowserAnimationsModule,

    SubPanelModule,

    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatRippleModule,
    MatInputModule,
    MatDatepickerModule,
    MatMomentDateModule,
  ],
  declarations: [TourSummaryComponent]
})
export class TourModule { }
