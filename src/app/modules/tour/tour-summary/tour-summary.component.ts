import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import {Tour} from '../../../models/tour';
import {MatSort, MatTableDataSource} from '@angular/material';
import {UpdateService} from '../../../services/rts/update.service';

@Component({
  selector: 'app-tour-summary',
  templateUrl: './tour-summary.component.html',
  styleUrls: ['./tour-summary.component.scss']
})
export class TourSummaryComponent implements OnInit, AfterViewInit {

  @ViewChild(MatSort) sort: MatSort;

  dataSource: any;
  //displayedColumns = ['Tours', 'Bus', 'Drivers', 'Status', 'Confirmed', 'Reserved', 'Available', 'Unpaid', 'Actions'];
  displayedColumns = ['Tours', 'Bus', 'Drivers', 'Confirmed', 'Reserved', 'Available', 'Actions'];

  tourList: Tour[] = [];

  constructor(private updateService: UpdateService) { }

  ngOnInit() {
    this.tourList.push(new Tour(true));
    this.tourList.push(new Tour(false));
    this.dataSource = new MatTableDataSource(this.tourList);
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    /** ToDo - find out how this supposed to work for data Objects **/
    // this.dataSource.filterPredicate = (data: Booking, filter: string) => data.participants[0].fullName.indexOf(filterValue) !== -1;

  }
}
