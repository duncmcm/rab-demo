import {Component, OnInit, Input, Renderer2, ElementRef, OnChanges} from '@angular/core';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-form-field-simple',
  templateUrl: './form-field-simple.component.html',
  styleUrls: ['./form-field-simple.component.css']
})
export class FormFieldSimpleComponent implements OnInit, OnChanges {

  @Input() hintText: string;
  @Input() text: string;
  @Input() disabled = false;
  @Input() value: any;
  @Input() prefix: string;
  @Input() width: number;

  placeholder = 'Pick ';

  constructor(private renderer: Renderer2, private el: ElementRef) { }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.text) {
      this.placeholder = this.placeholder + this.text;
    }
    if (!isNullOrUndefined(this.width)) {
      let sizeString = this.width.toString(10) + 'px';
      this.renderer.setStyle(this.el.nativeElement.children[0], 'width', sizeString);
    }
  }
}
