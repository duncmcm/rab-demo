import {Component, OnInit, Input, Output, OnChanges, EventEmitter, Renderer2, ElementRef} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-autocomplete-simple',
  templateUrl: './autocomplete-simple.component.html',
  styleUrls: ['./autocomplete-simple.component.css']
})
export class AutocompleteSimpleComponent implements OnInit, OnChanges {

  @Input() options = [];
  @Input() text = '';
  @Input() width: number;
  @Input() required = false;

  control: FormControl;
  filteredOptions: Observable<string[]>;
  placeholder = 'Pick ';

  @Output() onOptionSelected: EventEmitter<string> = new EventEmitter<string>();

  constructor(private renderer: Renderer2, private el: ElementRef) { }

  ngOnInit() {
  }

  ngOnChanges() {
    if (isNullOrUndefined(this.control)) {
      this.initControl();
    }
    if (this.text) {
      this.placeholder = this.placeholder + this.text;
    }
    if (!isNullOrUndefined(this.width)) {
      let sizeString = this.width.toString(10) + 'px';
      this.renderer.setStyle(this.el.nativeElement.children[0], 'width', sizeString);
    }
  }

  initControl() {
    if (this.required && this.required === true) {
      this.control = new FormControl('', [Validators.required, Validators.required]);
    } else {
      this.control = new FormControl();
    }

    this.filteredOptions = this.control.valueChanges.pipe(
      startWith(''),
      map(val => this.filter(val))
    );
  }

  getErrorMessage() {
    return this.control.hasError('required') ? 'You must enter a value' :
      this.control.hasError('email') ? 'Not a valid email' :
        '';
  }

  filter(val: string): string[] {
    return this.options.filter(option => option.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  onSelected(event: any) {
    this.onOptionSelected.next(event.option.value);
  }
}
