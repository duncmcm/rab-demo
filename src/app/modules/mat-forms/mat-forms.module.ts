import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteSimpleComponent } from './auto-complete/autocomplete-simple/autocomplete-simple.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule, MatInputModule, MatAutocompleteModule} from '@angular/material';
import { FormFieldSimpleComponent } from './field/form-field-simple/form-field-simple.component';

@NgModule({
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
  ],
  declarations: [AutocompleteSimpleComponent, FormFieldSimpleComponent],
  exports: [AutocompleteSimpleComponent, FormFieldSimpleComponent]
})
export class MatFormsModule { }
