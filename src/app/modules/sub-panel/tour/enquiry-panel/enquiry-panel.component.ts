import { Component, OnInit } from '@angular/core';
import {Tour} from '../../../../models/tour/Tour';
import {BasePanelComponent} from '../../base-panel/base-panel.component';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-enquiry-panel',
  templateUrl: './enquiry-panel.component.html',
  styleUrls: ['./enquiry-panel.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('void', style({height: '0px', minHeight: '0', visibility: 'hidden'})),
      state('*', style({height: '*', visibility: 'visible'})),
      transition('void <=> *', animate('250ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class EnquiryPanelComponent extends BasePanelComponent implements OnInit {

  tour: Tour;

  constructor() {
    super();
  }

  ngOnInit() {

  }

}
