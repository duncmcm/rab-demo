import { Component, OnInit, Input } from '@angular/core';
import {Tour} from '../../../../../../models/tour';
import {TourSummaryService} from '../../../../../../services/tour-summary.service';
import {TourEnquiryService} from '../../../../../../services/tour-enquiry.service';

@Component({
  selector: 'app-price-list-panel',
  templateUrl: './price-list-panel.component.html',
  styleUrls: ['./price-list-panel.component.css']
})
export class PriceListPanelComponent implements OnInit {

  @Input() tour: Tour;

  totalCost = 0;
  totalPax = 0;

  constructor(public tourEnquiryService: TourEnquiryService) { }

  ngOnInit() {
    this.tourEnquiryService.runningTotalAnnounce.subscribe(
      total => {
        this.totalCost = total;
      }
    );

    this.tourEnquiryService.runningPaxTotalAnnounce.subscribe(
      total => {
        this.totalPax = total;
      }
    );
  }

}
