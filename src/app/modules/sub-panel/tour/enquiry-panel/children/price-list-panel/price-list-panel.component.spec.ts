import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceListPanelComponent } from './price-list-panel.component';

describe('PriceListPanelComponent', () => {
  let component: PriceListPanelComponent;
  let fixture: ComponentFixture<PriceListPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PriceListPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceListPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
