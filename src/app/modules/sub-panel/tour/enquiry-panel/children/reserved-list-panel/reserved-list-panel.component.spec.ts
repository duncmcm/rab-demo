import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservedListPanelComponent } from './reserved-list-panel.component';

describe('ReservedListPanelComponent', () => {
  let component: ReservedListPanelComponent;
  let fixture: ComponentFixture<ReservedListPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservedListPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservedListPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
