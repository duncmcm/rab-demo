import { Component, OnInit, Input } from '@angular/core';
import {Tour} from '../../../../../../models/tour/Tour';

@Component({
  selector: 'app-reserved-list-panel',
  templateUrl: './reserved-list-panel.component.html',
  styleUrls: ['./reserved-list-panel.component.css']
})
export class ReservedListPanelComponent implements OnInit {

  @Input() tour: Tour;

  constructor() { }

  ngOnInit() {
  }

}
