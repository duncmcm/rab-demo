import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquiryStepPanelComponent } from './enquiry-step-panel.component';

describe('EnquiryStepPanelComponent', () => {
  let component: EnquiryStepPanelComponent;
  let fixture: ComponentFixture<EnquiryStepPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnquiryStepPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquiryStepPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
