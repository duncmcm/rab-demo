import { Component, OnInit, Input, OnChanges } from '@angular/core';
import {ListService} from '../../../../../../services/list.service';
import {Tour} from '../../../../../../models/tour';
import {TourSummaryService} from '../../../../../../services/tour-summary.service';
import {TourEnquiryService} from '../../../../../../services/tour-enquiry.service';

@Component({
  selector: 'app-enquiry-step-panel',
  templateUrl: './enquiry-step-panel.component.html',
  styleUrls: ['./enquiry-step-panel.component.scss']
})
export class EnquiryStepPanelComponent implements OnInit, OnChanges {

  @Input() tour: Tour;

  showAgent = false;
  showConcierge = false;
  showPax = true;
  showPackageAccommodation = false;
  showFullAccommodation = false; /** This will only eventually be used after a reservation has  been made **/
  showFirstChoiceRooms = false;
  showSecondChoiceRooms = false;

  adultsTotal = 0;
  seniorsTotal = 0;
  studentsTotal = 0;
  childrenTotal = 0;

  adultsPaxTotal = 0;
  seniorsPaxTotal = 0;
  studentsPaxTotal = 0;
  childrenPaxTotal = 0;

  constructor(public listService: ListService, public tourEnquiryService: TourEnquiryService) { }

  ngOnInit() {

  }

  ngOnChanges() {
    if (this.tour) {
      this.listService.tour = this.tour;
    }
  }

  setBookingType(event: any, refType: string) {
    console.log('setBookingReferences ' + event + ' ' + refType);
    this.showAgent = this.showConcierge = false;
    if (refType === 'Type' && event === 'Agent') {
      this.showAgent = true;
    } else if (refType === 'Type' && event === 'Concierge') {
      this.showConcierge = true;
    } else { /** Rabbies defaults **/
      this.showPax = true;
      this.showFullAccommodation = this.showPackageAccommodation = false;
    }
  }

  setBookingReference(event: any) {
    this.tour.reference = event;
  }

  /** setBookingAgent
   *  Also used to set showPax boolean after checking Agent type IE package or non-package
   * @param event
   * @param {string} refType
   */
  setBookingAgent(event: any) {
    if (event === 'Package') {
      this.showPax = this.showFullAccommodation = false;
      this.showPackageAccommodation = true;
      this.showFirstChoiceRooms = false;
    } else {
      this.showPax  = true;
    }
    this.tour.agent = event;
  }

  setBookingConcierge(event: any) {
    this.tour.concierge = event;
  }

  /** ToDo - Prices will come from API Call **/
  setPax(event: any, paxType: string) {
    switch (paxType) {
      case 'Adults':
        this.adultsTotal = event * 60;
        this.adultsPaxTotal = parseInt(event, 10);
        break;
      case 'Seniors':
        this.seniorsTotal = event * 40;
        this.seniorsPaxTotal = parseInt(event, 10);
        break;
      case 'Students':
        this.studentsTotal = event * 30;
        this.studentsPaxTotal = parseInt(event, 10);
        break;
      case 'Children':
        this.childrenTotal = event * 30;
        this.childrenPaxTotal = parseInt(event, 10);
        break;
    }
    this.tourEnquiryService.runningTotal = this.adultsTotal + this.seniorsTotal + this.studentsTotal + this.childrenTotal;
    this.tourEnquiryService.runningPaxTotal = this.adultsPaxTotal + this.seniorsPaxTotal + this.studentsPaxTotal + this.childrenPaxTotal;
  }

  setAccommodation(event: any, accType: string) {
    if (accType === 'First') {
      this.showFirstChoiceRooms = true;
    }
    if (accType === 'Second') {
      this.showSecondChoiceRooms = true;
    }
  }

  setAccommodationRoomType(event: any, accType: string) {

  }
}
