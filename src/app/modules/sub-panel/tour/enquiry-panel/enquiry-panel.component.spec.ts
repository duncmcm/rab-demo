import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquiryPanelComponent } from './enquiry-panel.component';

describe('EnquiryPanelComponent', () => {
  let component: EnquiryPanelComponent;
  let fixture: ComponentFixture<EnquiryPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnquiryPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquiryPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
