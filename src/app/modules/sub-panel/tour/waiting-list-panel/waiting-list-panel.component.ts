import { Component, OnInit, ViewChild } from '@angular/core';
import {Tour} from '../../../../models/tour';
import {BasePanelComponent} from '../../base-panel/base-panel.component';
import {Waiting} from '../../../../models/booking/Waiting';
import {CustomDatePipe} from '../../../../pipes/custom-date.pipe';
import {MatSort, MatTableDataSource} from '@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-waiting-list-panel',
  templateUrl: './waiting-list-panel.component.html',
  styleUrls: ['./waiting-list-panel.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('void', style({height: '0px', minHeight: '0', visibility: 'hidden'})),
      state('*', style({height: '*', visibility: 'visible'})),
      transition('void <=> *', animate('250ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class WaitingListPanelComponent extends BasePanelComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;

  tour: Tour;

  dataSource: any;
  dataSourceNew: any;
  displayedColumns = ['added', 'name', 'contact', 'pax', 'tour', 'tourDate', 'actions'];

  waitingList: Waiting[] = [];
  waitingListNew: Waiting[] = [];
  showNewWaitingRow = false;

  constructor(private datePipe: CustomDatePipe) {
    super();
  }

  ngOnInit() {
    this.waitingList.push(new Waiting(true, this.datePipe));
    this.waitingList.push(new Waiting(true, this.datePipe));
    this.waitingList.push(new Waiting(true, this.datePipe));
    this.waitingList.push(new Waiting(true, this.datePipe));
    this.waitingList.push(new Waiting(true, this.datePipe));
    this.dataSource = new MatTableDataSource(this.waitingList);

    this.waitingListNew.push(new Waiting(false, this.datePipe));
    this.dataSourceNew = new MatTableDataSource(this.waitingListNew);
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    /** ToDo - find out how this supposed to work for data Objects **/
    // this.dataSource.filterPredicate = (data: Booking, filter: string) => data.participants[0].fullName.indexOf(filterValue) !== -1;

  }

  showNewRow(event: any) {
    this.showNewWaitingRow = true;
  }
}
