import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaitingListPanelComponent } from './waiting-list-panel.component';

describe('WaitingListPanelComponent', () => {
  let component: WaitingListPanelComponent;
  let fixture: ComponentFixture<WaitingListPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaitingListPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitingListPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
