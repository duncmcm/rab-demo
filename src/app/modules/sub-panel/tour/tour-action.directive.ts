import {ComponentFactoryResolver, Directive, Input, ViewContainerRef, HostListener} from '@angular/core';
import {Router} from '@angular/router';
import {KeyboardService} from '../../../services/keyboard.service';
import {EnquiryPanelComponent} from './enquiry-panel/enquiry-panel.component';
import {PanelTemplate} from '../panel-template';
import {WaitingListPanelComponent} from './waiting-list-panel/waiting-list-panel.component';
import {NewBookingPanelComponent} from "../booking/new-booking-panel/new-booking-panel.component";

@Directive({
  selector: '[appTourAction]'
})
export class TourActionDirective {

  private subItem: any;

  @Input('appTourAction') config: any = {
    tour: ''
  };

  keyboardService: KeyboardService;
  constructor(public vcRef: ViewContainerRef, public cfr: ComponentFactoryResolver, public router: Router,
              keyboardsService: KeyboardService) {
    this.keyboardService = keyboardsService;
  }

  ngOnInit() {
    this.keyboardService.keyAnnounce.subscribe(
      key => {
        if (key === 'Escape') {
          this.vcRef.clear();
        }
      }
    );
  }

  @HostListener('click', ['$event'])
  onClick(event: any): void {
    this.vcRef.clear();
    if (event.target.id === 'enquiryIcon') {
      this.subItem = EnquiryPanelComponent;
      this.loadItem();
    }
    if (event.target.id === 'waitingIcon') {
      //this.subItem = WaitingListPanelComponent;
      this.subItem = NewBookingPanelComponent;
      this.loadItem();
    }
  }

  loadItem() {
    const item: PanelTemplate = new PanelTemplate(this.subItem, {tour: this.config.tour});
    const componentFactory = this.cfr.resolveComponentFactory(item.component);
    const componentRef = this.vcRef.createComponent(componentFactory);
    if (componentRef.instance instanceof EnquiryPanelComponent ) {
      (<EnquiryPanelComponent>componentRef.instance).tour = item.data.tour;
      (<EnquiryPanelComponent>componentRef.instance).clearParentContainer.subscribe(
        result => {
          if (result) {
            this.vcRef.clear();
          }
        }
      );
    }
    if (componentRef.instance instanceof WaitingListPanelComponent ) {
      (<WaitingListPanelComponent>componentRef.instance).tour = item.data.tour;
      (<WaitingListPanelComponent>componentRef.instance).clearParentContainer.subscribe(
        result => {
          if (result) {
            this.vcRef.clear();
          }
        }
      );
    }
    if (componentRef.instance instanceof NewBookingPanelComponent ) {
      (<NewBookingPanelComponent>componentRef.instance).tour = item.data.tour;
      (<NewBookingPanelComponent>componentRef.instance).clearParentContainer.subscribe(
          result => {
            if (result) {
              this.vcRef.clear();
            }
          }
      );
    }
  }
}
