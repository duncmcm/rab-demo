import { Component, OnInit, Input, OnChanges } from '@angular/core';
import {ListService} from '../../../../services/list.service';
import {Tour} from '../../../../models/tour/Tour';

@Component({
  selector: 'app-accommodation-panel',
  templateUrl: './accommodation-panel.component.html',
  styleUrls: ['./accommodation-panel.component.scss']
})
export class AccommodationPanelComponent implements OnInit, OnChanges {

  @Input() tour: Tour;

  showPackageAccommodation = false;
  showFullAccommodation = true;
  showFirstChoiceRooms = false;
  showSecondChoiceRooms = false;

  constructor(public listService: ListService) { }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.tour) {
      this.listService.tour = this.tour;
    }
  }

  setAccommodation(event: any, accType: string) {
    if (accType === 'First') {
      this.showFirstChoiceRooms = true;
    }
    if (accType === 'Second') {
      this.showSecondChoiceRooms = true;
    }
  }

}
