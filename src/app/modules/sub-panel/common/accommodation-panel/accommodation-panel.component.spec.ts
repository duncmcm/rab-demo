import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccommodationPanelComponent } from './accommodation-panel.component';

describe('AccommodationPanelComponent', () => {
  let component: AccommodationPanelComponent;
  let fixture: ComponentFixture<AccommodationPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccommodationPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccommodationPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
