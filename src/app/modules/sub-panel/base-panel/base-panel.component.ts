import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-base-panel',
  templateUrl: './base-panel.component.html',
  styleUrls: ['./base-panel.component.css']
})
export class BasePanelComponent implements OnInit {

  @Output() public clearParentContainer = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }

}
