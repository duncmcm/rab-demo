import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBookingPanelComponent } from './new-booking-panel.component';

describe('NewBookingPanelComponent', () => {
  let component: NewBookingPanelComponent;
  let fixture: ComponentFixture<NewBookingPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewBookingPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBookingPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
