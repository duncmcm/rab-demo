import { Component, OnInit } from '@angular/core';
import {Tour} from '../../../../models/tour/Tour';
import {BasePanelComponent} from '../../base-panel/base-panel.component';

@Component({
  selector: 'app-new-booking-panel',
  templateUrl: './new-booking-panel.component.html',
  styleUrls: ['./new-booking-panel.component.scss']
})
export class NewBookingPanelComponent extends BasePanelComponent implements OnInit {

  tour: Tour;

  step = 0;

  constructor() {
    super();
  }

  ngOnInit() {
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
}
