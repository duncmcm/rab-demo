import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WaitingListPanelComponent } from './tour/waiting-list-panel/waiting-list-panel.component';
import { EnquiryPanelComponent } from './tour/enquiry-panel/enquiry-panel.component';
import { NewBookingPanelComponent } from './booking/new-booking-panel/new-booking-panel.component';
import { TourActionDirective } from './tour/tour-action.directive';
import { BasePanelComponent } from './base-panel/base-panel.component';
import {
  MatFormFieldModule,
  MatExpansionModule,
  MatInputModule,
  MatStepperModule,
  MatListModule,
  MatButtonModule,
  MatAutocompleteModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatRippleModule
} from '@angular/material';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { PriceListPanelComponent } from './tour/enquiry-panel/children/price-list-panel/price-list-panel.component';
import { ReservedListPanelComponent } from './tour/enquiry-panel/children/reserved-list-panel/reserved-list-panel.component';
import { EnquiryStepPanelComponent } from './tour/enquiry-panel/children/enquiry-step-panel/enquiry-step-panel.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormsModule} from '../mat-forms/mat-forms.module';
import {UtilitiesModule} from '../utilities/utilities.module';
import {WaitingListService} from '../../services/waiting-list.service';
import {CustomDatePipe} from '../../pipes/custom-date.pipe';
import { AccommodationPanelComponent } from './common/accommodation-panel/accommodation-panel.component';
import { AccountPanelComponent } from './booking/new-booking-panel/children/account-panel/account-panel.component';

@NgModule({
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,

    MatFormFieldModule,
    MatExpansionModule,
    MatInputModule,
    MatStepperModule,
    MatListModule,
    MatButtonModule,
    MatAutocompleteModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatRippleModule,
    MatInputModule,

    MatFormsModule,
    UtilitiesModule,

    BrowserModule,
    BrowserAnimationsModule
  ],
  declarations: [WaitingListPanelComponent, EnquiryPanelComponent, NewBookingPanelComponent, TourActionDirective,
                 BasePanelComponent, PriceListPanelComponent, ReservedListPanelComponent, EnquiryStepPanelComponent,
                 AccommodationPanelComponent,
                 AccountPanelComponent],
  exports: [WaitingListPanelComponent, EnquiryPanelComponent, NewBookingPanelComponent, TourActionDirective,
            BasePanelComponent, PriceListPanelComponent, ReservedListPanelComponent, EnquiryStepPanelComponent,
            AccommodationPanelComponent],
  entryComponents: [WaitingListPanelComponent, EnquiryPanelComponent, NewBookingPanelComponent, BasePanelComponent],
  providers: [WaitingListService, CustomDatePipe]
})
export class SubPanelModule { }
