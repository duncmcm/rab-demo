import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { DynamicFieldDirective } from './components/dynamic-field.directive';
import { FormButtonComponent } from './components/form-button/form-button.component';
import { DynamicFormComponent } from './containers/dynamic-form/dynamic-form.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [DynamicFieldDirective, FormButtonComponent, DynamicFormComponent],
  exports: [ DynamicFormComponent ],
  entryComponents: [ FormButtonComponent ]
})
export class DynamicFormModule { }
