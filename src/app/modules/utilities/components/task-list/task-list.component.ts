import {Component, ElementRef, OnInit, Renderer2, OnDestroy} from '@angular/core';
import {AppStateService} from '../../../../services/app-state.service';
import {TaskListService} from '../../../../services/task-list.service';
import {Task} from '../../../../models/utilities/task';
import {KeyboardService} from '../../../../services/keyboard.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit, OnDestroy {

  tasks: Task[];

  subTask: Subscription;
  subKey: Subscription;

  constructor(private renderer: Renderer2, private el: ElementRef,
              private appStateService: AppStateService, private taskListService: TaskListService,
              private keyBoardService: KeyboardService) { }

  ngOnInit() {
    const pos = (this.appStateService.showTaskListAtPositionX - 40) + 'px';
    this.renderer.setStyle(this.el.nativeElement.children[0], 'left', pos);

    this.subTask = this.taskListService.taskListAnnoounce.subscribe(
      list => {
        this.tasks = list;
      }
    );

    this.subKey = this.keyBoardService.keyAnnounce.subscribe(
      key => {
        if (key === 'Escape') {
          this.ngOnDestroy();
        }
      }
    );

    this.taskListService.getTasks();
  }

  ngOnDestroy() {
    this.subKey.unsubscribe();
    this.subTask.unsubscribe();
  }
}
