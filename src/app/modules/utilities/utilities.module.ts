import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HaloDirective } from './directives/halo.directive';
import { TaskListComponent } from './components/task-list/task-list.component';
import { TaskPanelComponent } from './components/task-list/task-panel/task-panel.component';
import {ContextMenuModule} from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,

    ContextMenuModule
  ],
  declarations: [HaloDirective, TaskListComponent, TaskPanelComponent],
  exports: [HaloDirective, TaskListComponent, TaskPanelComponent]
})
export class UtilitiesModule { }
