import {Directive, ElementRef, Input, OnInit, Renderer2, OnChanges} from '@angular/core';

@Directive({
  selector: '[appHalo]'
})
export class HaloDirective implements OnInit, OnChanges {

  /** HaloDirective - Used to provide A Halo effect to indicate changes or addition/deletion of info,
   *  including price changes, tasks, booking additions etc.
   *
   * @type {{task: string; once: boolean; compareValue: number; parent: boolean}}
   */
  @Input('appHalo') config: any = {
    task: '',
    once: false,
    compareValue: 0,
    parent: false
  };

  compareValueWith = 0;

  constructor(private el: ElementRef, private renderer: Renderer2) {
  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.clearStyles();
    if ((this.config.task && this.config.task.priority === 1) || this.config.any) {
      this.renderer.addClass(this.el.nativeElement.children[0], 'pulse');
    }
    if (this.config.once) {
      this.renderer.addClass(this.el.nativeElement.children[0], 'pulse-once');
    }
    if (this.compareValueWith !== this.config.compareValue) {
      if (this.config.parent) {
        this.renderer.addClass(this.el.nativeElement, 'pulse-once');
      } else {
        this.renderer.addClass(this.el.nativeElement.children[0], 'pulse-once');
      }
    }
    this.compareValueWith = this.config.compareValue;
  }

  clearStyles() {
    this.renderer.removeClass(this.el.nativeElement.children[0], 'pulse');
    this.renderer.removeClass(this.el.nativeElement.children[0], 'pulse-once');
    this.renderer.removeClass(this.el.nativeElement, 'pulse');
    this.renderer.removeClass(this.el.nativeElement, 'pulse-once');

    /** This is a trick to trigger a re-render which triggers the animation again
     *  Next line is just to get past TSLint ! **/
    let width = this.el.nativeElement.children[0].offsetWidth;
    width = 0;
  }
}
