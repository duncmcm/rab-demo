import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppComponent } from './app.component';
import { TopNavComponent } from './components/top-nav/top-nav.component';
import { HttpModule } from '@angular/http';

import { MatMenuModule } from '@angular/material';
import { MenubarModule } from 'primeng/menubar';
import {TourModule} from './modules/tour/tour.module';
import {UtilitiesModule} from './modules/utilities/utilities.module';
import {TaskListService} from './services/task-list.service';
import {AppStateService} from './services/app-state.service';
import {RouterModule, Routes} from '@angular/router';
import {UpdateService} from './services/rts/update.service';
import {KeyboardService} from './services/keyboard.service';
import {ListService} from './services/list.service';
import {FormsModule} from '@angular/forms';
import {TourEnquiryService} from './services/tour-enquiry.service';
import {ConfigService} from './services/rts/config.service';
import {DynamicFormModule} from './modules/dynamic-form/dynamic-form.module';

import {NgRedux, NgReduxModule, DevToolsExtension } from '@angular-redux/store'; // <- New
import { rootReducer, IAppState, INITIAL_STATE } from './store/store';
import { AppActions } from './app.actions';

const appRoutes: Routes = [
  //{ path: 'tours', component: MainAgentsComponent},
  //{ path: 'general-admin', component: GeneralAdminComponent}
];

@NgModule({
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  declarations: [
    AppComponent,
    TopNavComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MatMenuModule,
    HttpModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes, {useHash: false}),
    MenubarModule,
    TourModule,
    UtilitiesModule,
    DynamicFormModule,
    NgReduxModule
  ],
  providers: [TaskListService, AppStateService, UpdateService, KeyboardService, ListService, TourEnquiryService,
  ConfigService,
  AppActions],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(ngRedux: NgRedux<IAppState>, devTools: DevToolsExtension) {
    // Tell @angular-redux/store about our rootReducer and our initial state.
    // It will use this to create a redux store for us and wire up all the
    // events.
    const storeEnhancers = devTools.isEnabled() ? // <- New
      [ devTools.enhancer() ] : // <- New
      []; // <- New

    ngRedux.configureStore(rootReducer, INITIAL_STATE, [], storeEnhancers);
  }
}
