import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import {AppStateService} from '../../services/app-state.service';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss']
})
export class TopNavComponent implements OnInit {

  items: MenuItem[];
  adminItems: MenuItem[];

  constructor(private appStateService: AppStateService) { }

  ngOnInit() {
    this.items = [
      {
        label: 'Tours',
        icon: 'fa fa-bus',
        items: [
          {label: 'Edinburgh'},
          {label: 'Glasgow'},
          {label: 'London'},
          {label: 'Dublin'},
          {label: 'Inverness'},
        ]
      }
    ];

    this.adminItems = [
      {
        label: 'Admin',
        icon: 'fa fa-cog',
        items: [
          {label: 'General'},
          {label: 'Agents'},
          {label: 'Accommodation',
            items: [
              {label: 'Provider Reports', icon: 'fa-table'},
              {label: 'Redo', icon: 'fa-table'}
            ]
          },
          {label: 'Point Of Sale',
            items: [
              {label: 'Transaction List', icon: 'fa-credit-card'},
              {label: 'Daily Cash Up', icon: 'fa-credit-card'}
            ]}
        ]
      }
    ];
  }

  onMouseOverAction(event: any) {
    this.appStateService.showTaskListAtPositionX = event.x;
  }

  onMouseOut(event: any) {
    // this.appStateService.showTaskListAtPositionX = 0;
  }

}
