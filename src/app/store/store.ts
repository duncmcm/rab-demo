import { Action } from 'redux';
import {AppActions} from '../app.actions';

export interface IAppState {
  account: Account;
  count: number;
}

export const INITIAL_STATE: IAppState = {
  account: null,
  count: 0
};

export function rootReducer(lastState: IAppState, action: Action): IAppState {
  switch (action.type) {
    case AppActions.LOGIN: return { count: lastState.count + 1, account: null };
    case AppActions.LOGOUT: return { count: lastState.count - 1, account: null };
  }

  // We don't care about any other actions right now.
  return lastState;
}
