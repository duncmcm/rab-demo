import {Component, OnInit} from '@angular/core';
import {AppStateService} from './services/app-state.service';
import {Router} from '@angular/router';
import {ConfigService} from "./services/rts/config.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  appStateService: AppStateService;
  constructor(private router: Router, appStateService: AppStateService, configService: ConfigService) {
    this.appStateService = appStateService;
  }

  ngOnInit() {
    this.router.navigate(['tours']);
  }
}
