import {AccommodationPreference} from './AccommodationPreference';
import {SpotItem} from './utilities/spotitem';

export class Accommodation extends SpotItem {

  ticket: number;
  name: string;
  choices: AccommodationPreference[];
  bookedChoiceString: string;
  cancelled = false;

  private _type: string;

  set type(value: string) {
    this._type = value;
    if (value === 'package') {
      this.package = true;
      this.combi = false;
    } else {
      this.package = false;
      this.combi = true;
    }
  }

  get type() {
    return this._type;
  }

  package = false;
  combi = false;

  pax: number;
  dates: Date[];


  // Accommodation currently gets send as a big long url in the format:
  // /api/booking/setAccommodationPrefs/bookingId/{bookingId}/diyai/{diyAccommodationInfo}/c1/{firstChoice}/c2/
    // {secondChoice}/dbl/{dbl}/sngl/{sngl}/twn/{twn}/dps/{dps}/mhok/{mhok}/policy/{policy}/othReqs/{othReqs}/m/{m}/f/{f}
  bookingId: string;
  diyai: string;
  c1: string;
  c2: string;
  m: number;
  f: number;
  dbl: number;
  sngl: number;
  twn: number;
  dps: number;
  mhok: boolean;
  policy: boolean;
  othReqs: string;

  constructor() {
	  super();
  }

}
