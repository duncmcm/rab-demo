import {SpotItem} from '../utilities/spotitem';
import {AccommodationPreference} from '../AccommodationPreference';
import {Account} from '../account/account';
import {Participant} from './Participant';
import {TourDeparture} from './TourDeparture';

export class Booking extends SpotItem {

  /** Base Attributes **/

  id: number = null;
  reservationGroupId: number;
  sourceSystem: number;
  agentCode? = '';
  proxyAgent? = '';

  private _tourCode: string;
  get tourCode(): string {
      return this._tourCode;
  }

  set tourCode(value: string) {
      this._tourCode = this.description = value;
      this.code = this.id.toString();
  }

  numAdults? = 0;
  numKids? = 0;
  numStudents? = 0;
  numSeniors? = 0;

  upsold = false;
  mobile = 0;
  promoCode? = '';
  concierge? = '';
  conciergeHotel? = '';
  specialReq? = '';
  howReferred = '';
  confEmail? = '';

  /** Other Objects **/
  account: Account = new Account();
  participants: Participant[] = [];
  accommodationPreference: AccommodationPreference = new AccommodationPreference();
  tourDeparture: TourDeparture = new TourDeparture();

  constructor () {
    super();
    this.account.initialiseContactInfo();
    this.objType = 'booking';
    this.iconForType = 'fa-ticket';
  }
}
