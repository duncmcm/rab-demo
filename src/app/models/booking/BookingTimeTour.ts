import {TourType} from './TourType';
import {PricingCurrency} from './PricingCurrency';
import {TourDeparture} from './TourDeparture';

export class BookingTimeTour {

	id: number;
	tourCode: string;
	type: TourType;
	sequence: number;
	currency: PricingCurrency;
	fullPrice: number;
	concessionPrice: number;
	fullCost: number;
	concessionCost: number;
	promoReduction: number;
	surcharges: number;
	bus: number;
	tourDeparture: TourDeparture;
	departurePointAcknowledged: boolean;
	tourDays: number;
	departureDate: string;
	departureTime: string;
	returnTime: string;

	constructor() {

	}
}
