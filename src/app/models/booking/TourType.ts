
export class TourType {

	static readonly CITY = 'City';
	static readonly SINGLE_DAY = 'Single Day';
	static readonly MULTI_DAY = 'Multi Day';
	static readonly COMBO = 'Combo';

	constructor() {

	}
}
