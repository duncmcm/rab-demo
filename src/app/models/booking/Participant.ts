import {ContactInfo} from '../account/ContactInfo';

export class Participant {

  firstName: string;
  private _lastName: string;
  protected fullName: string;
  contactInfo: ContactInfo[] = [];
  lead: boolean;

  get lastName(): string {
    return this._lastName;
  }

  set lastName(value: string) {
    this._lastName = value;
    this.fullName = this.firstName + ' ' + this._lastName;
  }

  constructor() {
  }

  protected addToContactInfo(type: string, data: any) {
    let info = new ContactInfo();
    info.type = type;
    info.setAllData(data);
    this.contactInfo.push(info);
  }

  updateContactInfo(index: number, data: any) {
    this.contactInfo[index].setAllData(data);
  }
}
