import {Participant} from './Participant';
import getRandomName from 'namesgenerator';
import {ContactInfo} from '../account/ContactInfo';
import {Tour} from '../tour/Tour';
import {CustomDatePipe} from '../../pipes/custom-date.pipe';

export class Waiting extends Participant {

  dateAdded: string;
  pax: number;
  tour: Tour;

  constructor(dummy: boolean, private datePipe: CustomDatePipe) {
    super();
    if (dummy) {
      this.initDummyInstance();
    }
  }

  initDummyInstance() {
    this.fullName = getRandomName();
    this.pax = this.getRandomInt(1, 5);
    this.addToContactInfo(ContactInfo.EMAIL, 'fred@yahoo.co.uk');
    this.tour = new Tour(this.datePipe);
    this.tour.tourDeparture.tourCode = '3DSKY';
    this.tour.tourDeparture.tourDate = '21-02-2018';
    this.dateAdded = '20-02-2018';
  }

  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}
