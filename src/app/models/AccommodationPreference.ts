import {BedModel} from './BedModel';

export class AccommodationPreference {

	firstChoice? = '';
	secondChoice? = '';
	diyAccommodationInfo? = '';
	maleCount? = 0;
	femaleCount? = 0;
	mixedHostelDormOK = false;
	otherRequirements = '';
	policyConfirmed = true;

	bedModel: BedModel = new BedModel();

	constructor() {

	}
}
