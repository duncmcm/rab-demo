import {Person} from './person';
import {Address} from './Address';
import {ContactInfo} from './ContactInfo';

export class Account {

	id: number = null;

	person: Person = new Person();
	addresses: Address = new Address();
	contactInfo: ContactInfo[] = [];

	construtor() {
	}

	initialiseContactInfo() {
		let email = new ContactInfo();
		email.type = ContactInfo.EMAIL;
		this.contactInfo.push(email);

		let mobile = new ContactInfo();
		mobile.type = ContactInfo.MOBILE;
		this.contactInfo.push(mobile);

		let phone = new ContactInfo();
		phone.type = ContactInfo.LOCAL_PHONE;
		this.contactInfo.push(phone);
	}
}
