import {SpotItem} from '../utilities/spotitem';

export class ContactInfo extends SpotItem {

  static readonly EMAIL = 'EMAIL';
  static readonly MOBILE = 'MOBILE';
  static readonly LOCAL_PHONE = 'LOCAL_PHONE';

  id: number;

  data: any;

  type: string;

  constructor() {
    super();
    this.objType = 'contact';
    this.iconForType = 'fa-user';
  }

  public setAllData(value: any) {
    this.description = this.data = value;
    this.code = this.objType;
  }
}
