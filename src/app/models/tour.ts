/**
 * Created by duncmcm on 14/02/2018.
 */
import getRandomName from 'namesgenerator';

export class Tour {

    id = this.getRandomInt(10000, 50000);
    bus = this.getRandomInt(1, 50);
    driver = getRandomName();

    confirmed = this.getRandomInt(1, 5);
    reserved = this.getRandomInt(1, 5);
    available = 16 - this.confirmed - this.reserved;
    unpaid = this.getRandomInt(1, 16);

    /** These will be replaced by data from API call **/
    totalPax = 0;
    totalCost = 0;

    multiDay = false;

    reference: string;
    agent: string;
    concierge: string;

    constructor(multi: boolean) {
      this.multiDay = multi;
    }

    getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}
