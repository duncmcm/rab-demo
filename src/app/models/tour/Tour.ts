import {CustomDatePipe} from '../../pipes/custom-date.pipe';
import {SpotItem} from '../utilities/spotitem';
import {TourDeparture} from '../booking/TourDeparture';
import getRandomName from 'namesgenerator';

export class Tour extends SpotItem {

  /** comment out when testing **/

  // id: string;
  // driver: string;
  // unpaid: number;
  // reserved: number;
  // private _confirmed: number;
  // available: number;
  //
  // get confirmed(): number {
  // 	return this._confirmed;
  // }
  //
  // set confirmed(value: number) {
  // 	this._confirmed = value;
  // 	this.available = this.capacity - this.confirmed - this.reserved;
  // 	if (this.available === 0) {
  // 		this.canBook = false;
  // 	} else {
  // 		this.canBook = true;
  // 	}
  // }

  // private _tourCode: string;
  // private _tourDate: Date;
  // get tourDate(): Date {
  // 	return this._tourDate;
  // }
  //
  // set tourDate(value: Date) {
  // 	this._tourDate = value;
  // 	this.departureDate = this.formatDate(this.tourDate);
  // }
  //
  // get tourCode(): string {
  // 	return this._tourCode;
  // }
  //
  // set tourCode(value: string) {
  // 	this._tourCode = this.code = value;
  // }

  /** **/

  city: string;

  departureDate: string;
  busNo: number;

  capacity: number;
  total: number;
  canBook: boolean;

  tourDeparture: TourDeparture = new TourDeparture();

  onHold: boolean;
  closedOut: boolean;

  private _tourTitle: string;
  action: string;

  accomodationAction: boolean;
  accommodationIssue: boolean;

  driverTraining: boolean;
  tourNotes: boolean;

  datePipe: CustomDatePipe;

  /** Test Data **/
  id = this.getRandomInt(10000, 50000);
  bus = this.getRandomInt(1, 50);
  driver = getRandomName();
  confirmed = this.getRandomInt(1, 5);
  reserved = this.getRandomInt(1, 5);
  available = 16 - this.confirmed - this.reserved;
  unpaid = this.getRandomInt(1, 3);

  /** These will be replaced by data from API call **/
  totalPax = 0;
  totalCost = 0;
  multiDay = false;
  reference: string;
  agent: string;
  concierge: string;

  tourCode = 'WHIS';
  tourDate = '2018-02-21';

  /** End of Test Data **/

  constructor(datePipe: CustomDatePipe, multi?: boolean) {
    super();
    this.datePipe = datePipe;
    this.objType = 'tour';
    this.iconForType = 'fa-bus';

    if (multi) {

    }
  }

  get tourTitle(): string {
    return this._tourTitle;
  }

  set tourTitle(value: string) {
    this._tourTitle = this.description = value;
  }

  public formatDate(date) {
    return this.datePipe.transform(date, 'dbISO');
  }
}
