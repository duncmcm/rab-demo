import {Tour} from './Tour';
import {SpotItem} from '../utilities/spotitem';

export class Route extends SpotItem {

	id: number;

	private _tour: Tour;

	constructor() {
		super();
		this.objType = 'route';
		this.iconForType = 'fa-map';
	}

	get tour(): Tour {
		return this._tour;
	}

	set tour(value: Tour) {
		this._tour = value;
		this.code = this._tour.tourCode;
		this.description = this.tour.tourTitle;
	}
}
