export class SpotItem {

  code: string;
  description: string;
  objType: string;
  iconForType: string;

  constuctor() {

  }

  trimJson() {
    delete this.code;
    delete this.description;
    delete this.objType;
    delete this.iconForType;
  }

  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}
