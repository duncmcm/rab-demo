export class Task {

  id: number;

  taskType: string;
  description: string;
  ownerName: string;
  ownerGroup: string;
  route: string;

  /** used to style the associated UI element to indicate priority Task **/
  priority: number;

  /** used to style the associated UI element to indicate added or deleted Task **/
  action: string;

  constructor() {

  }

}
