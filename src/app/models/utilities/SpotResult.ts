import {Tour} from '../tour/Tour';
import {Booking} from '../booking/Booking';
import {Accommodation} from '../Accommodation';
import {Route} from '../tour/route';
import * as _ from 'underscore';
import {ContactInfo} from '../account/ContactInfo';

export class SpotResult {

	tours: Tour[] = [];
	bookings: Booking[] = [];
	accommodations: Accommodation[] = [];
	routes: Route[] = [];
	contactInfo: ContactInfo[] = [];

	filtered: any[] = [];

	constructor() {

	}

	/** Filters ALL any[] of this object by 'term'
	 *  IE we let this function define, for each object type WHAT we
	 *  filter on. IE:
	 *  tour => filter on tourCode
	 *  route => filter on route.tour.tourCode
	 *  booking => filter on bookingId
	 *  accommodation => filter on 'name' AND 'addressLine1'
	 *  contactInfo => filter on 'data'
	 *  etc
	 * @param {string} term
	 */
	filter(term: string) {
		this.tours = _.filter(this.tours, function(tour) {
			return tour.tourCode.indexOf(term) !== -1;
		});
		this.routes = _.filter(this.routes, function(route) {
			return route.tour.tourCode.indexOf(term) !== -1;
		});
		this.contactInfo = _.filter(this.contactInfo, function(info) {
			return info.data.indexOf(term) !== -1;
		});
		this.bookings = _.filter(this.bookings, function(booking) {
			return booking.id.toString().indexOf(term) !== -1;
		});
	}

	isEmpty(): boolean {
		if (this.tours.length === 0 && this.bookings.length === 0 && this.accommodations.length === 0
			&& this.routes.length === 0 && this.contactInfo.length === 0 && this.bookings.length === 0) {
			return true;
		}
		return false;
	}
}
