import { Injectable } from '@angular/core';
import { Action } from 'redux';

@Injectable()
export class AppActions {
  static LOGIN = 'LOGIN';
  static LOGOUT = 'LOGOUT';

  login(): Action {
    return { type: AppActions.LOGIN };
  }

  logout(): Action {
    return { type: AppActions.LOGOUT };
  }
}
