import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Tour} from '../models/tour/Tour';
import {Subject} from 'rxjs/Subject';
import { environment } from '../../environments/environment';
import {CustomDatePipe} from '../pipes/custom-date.pipe';
import {HeaderConfig} from './headers/HeaderConfig';

@Injectable()
export class TourSummaryService {

  static readonly MULTI_DAY: string[] = ['3DSKY', '3DSKY/2', '2DAYLNH', '5DIONA', '5DHIGH', '5DORK', '3DLKD', '4DISLAY',
      'G3DSKY', 'G2DAYLNH', '5DNENG', '3DSPEY', '5DWIRE', '4DSCEX', 'E2CBO', 'E5DCORN', 'E3HENW', 'E3SWH', '6DHEBSKY',
      '5DNIRE', '4DSKWH', '3DAAAC', '4DMULL', '6DCOHEB', 'E5HWYC', 'E4HNWL', '3DSEIRE', '3DWCIRE', '4DOUT', '4DFOOD',
      'E3KENT', '3DNORTH', '4DCGMIRE', '4DKERC', 'E5FRAN', '3DINVORK', 'E3LAKES', 'E5WALES', 'G3DAAAC', 'E3SHAKE'];

  static readonly EURO_TOURS: string[] = ['5DWIRE', '5DNIRE', '3DSEIRE', '3DWCIRE', '3DNORTH', '4DCGMIRE', '4DKERC'];

  tourData: any;
  public parsedToursSource = new Subject<Tour[]>();
  public parsedToursAnnounce = this.parsedToursSource.asObservable();

  APIUrl = environment.API_URL + '/tour/daySummary';
  localUrl = '/tour/daySummary';

  constructor(private http: Http, private datePipe: CustomDatePipe, private headerConfig: HeaderConfig) {}

  getSummary(summaryDate: string, city: string) {
	  let body = this.createPostBody(summaryDate, city);
	  this.http.post(this.APIUrl, body, {headers: this.headerConfig.createDefaultHeader()})
          .map(response => response.json().data)
          .subscribe(
              data => this.tourData = data,
              err => this.logError(err),
              () => this.parseData(this.tourData)
          );
  }

  logError(error: any) {
      console.log('Tour API Call Error ' + error);
  }

  createPostBody(date: string, city: string) {
      let b =  {'departureDate': this.datePipe.transform(date, 'forDB'), 'city': city };
      console.log('TourSummaryService BODY ' + b);
      return b;
  }

  parseData(data: any): Tour[] {
    console.log('Tour API Call completed OK');
    let tours: Tour[] = [];
    for (let i = 0; i < data.length; i++) {
        let tour = new Tour(this.datePipe);
        tour.id = data[i].id;
        tour.closedOut = data[i].closed;
        // tour.onHold = data[i].onHold;
        tour.driver = data[i].driver;

        tour.reserved = data[i].reserved;
        tour.capacity = data[i].capacity;  /** Load capacity before total so that tour.canBook is set correctly **/
        // tour.total = data[i].total;
        tour.confirmed = data[i].confirmed;

        tour.tourCode = data[i].key.tourCode;
        tour.tourDate = data[i].key.departureDate;
		tour.busNo = data[i].key.bus;

		if (data[i].key.bus > 1) {
			tour.tourCode = tour.tourCode + '/' + tour.busNo.toString();
    }

		tour.unpaid = data[i].unpaid;
        tour.tourTitle = data[i].title;
        tour.driverTraining = true; /** data[i].driverTraining; **/
        tour.accomodationAction = true; /** data[i].accomodationAction; **/
        tour.accommodationIssue = true;  /** ToDo - need real data here just set to true for testing purposes **/
        tour.tourNotes = true; /** data[i].tourNotes; **/
        tours.push(tour);
    }

    this.parsedToursSource.next(tours);
    return tours;
  }

}
