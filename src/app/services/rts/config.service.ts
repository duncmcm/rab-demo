import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ConfigService {

  /** ConfigService.
   *  - Interfaces with the ConfigFileService in back-end which monitor config file changes.
   *  - Individual config files will be dedicated to individual UI panels
   *    and have 'panel', 'row' and 'items' configuraiton information
   *  - Messages will be of the form : {messageType: string, data: any}
   */
  messageType: string;
  data: any;
  socket: any;

  constructor() {
    console.log('Config UI Service');
    this.socket = io('http://localhost:2370');
    this.socket.on('passengerList-config', data => this.parsePassengerListConfig(data) );
    this.socket.on('accountPanel-config', data => this.parseAccountPanelConfig(data) );

  }

  parsePassengerListConfig(data: any) {
    console.log('passengerList-config: ' + data.config.name);
  }

  parseAccountPanelConfig(data: any) {
    console.log('accountPanel-config: ' + data.config.name);
  }
}
