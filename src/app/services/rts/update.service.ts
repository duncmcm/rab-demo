import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class UpdateService {

  /** UpdateService.
   *  - Interfaces with the UpdateService in back-end which receives messages from ActiveMQ.
   *  - Messages will provide update information for the following objects:
   *    - Tour, Booking, Task, Accommodation, Allocation
   *  - Messages will be of the form : {messageType: string, data: any}
   *  - See /microservices/update/README.md for a list of messageTypes and their format
   */
  messageType: string;
  data: any;
  socket: any;

  constructor() {
    console.log('Update Service Init');
    this.socket = io('http://localhost:2360');
    this.socket.on('task_ui_updates', data => this.parseReservations(data) );
  }

  parseReservations(data: any) {
    console.log('New Task Message Received');
  }
}
