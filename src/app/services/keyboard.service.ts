import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class KeyboardService {

  /** KeyboardService
   *  Used to manage normal and hot key functionality within the app
   */

  /** Digit Key constants **/
  static readonly DIGIT_1 = 43;

  /** Character Key constants **/
  static readonly CHAR_S = 83;

  /** Character Key constants **/
  static readonly ESCAPE = 27;
  static readonly KEY_DOWN = 40;
  static readonly KEY_UP = 38;
  static readonly SPACE = 32;

  keySource = new Subject<string>();
  public keyAnnounce = this.keySource.asObservable();

  indexSelectSource = new Subject<number>();
  public indexSelectAnnounce = this.indexSelectSource.asObservable();
  indexSelected = 0;

  constructor() {
    document.addEventListener('keyup', (e: KeyboardEvent) => this.sendKeyObservable(e));
  }

  sendKeyObservable(key) {
    console.log('Keyboard Keycode is ' + key.keyCode);
    if (key.keyCode === KeyboardService.ESCAPE) {
      this.keySource.next(key.key); /** Generic Escape event **/
    }
    if (key.ctrlKey && key.keyCode === KeyboardService.SPACE) { /** Combination for showing Spotlight Search **/
	  this.keySource.next('spotlight');
	}

	if (key.keyCode === KeyboardService.KEY_DOWN) { /** Combination for stepping through Spotlight Search Results if being shown **/
	  this.keySource.next('keyDown');
	  this.indexSelected++;
	  this.indexSelectSource.next(this.indexSelected);
    } else if (key.keyCode === KeyboardService.KEY_UP) { /** Combination for stepping through Spotlight Search Results if being shown **/
	  this.keySource.next('keyUp');
	  this.indexSelected--;
	  this.indexSelectSource.next(this.indexSelected);
	} else {
		this.indexSelected = 0;
	}
  };

}
