import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {Subject} from 'rxjs/Subject';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map';
import {Tour} from '../models/tour';

@Injectable()
export class ListService {


  /** ToDo - Change these hardcoded lists to API Calls **/
  bookingType = ['Rabbies', 'Agent', 'Concierge'];
  bookingRefs = ['Email', 'Telephone', 'Counter', 'Cafe', 'Fam', 'Press', 'Training'];
  agentType = ['Non-Package', 'Package'];
  conciergeList = ['Concierge Tom', 'Concierge Dick', 'Concierge Harry'];

  tourAvailability = [];
  roomCount = [];  /** Just a count from 1 to 5 for now - for room count selection for accommodation **/

  accommChoices = ['Hotel', 'Hostel', 'B&B Standard', 'B&B En-Suite', 'Room Only',
    'Hotel 1 Star', 'Hotel 2 Star', 'Hotel 3 Star', 'Hotel 4 Star', 'Hotel 5 Star',
    'Client Booked', 'Agent Booked', 'No Choice'];
  accommChoicesAgent = ['Hotel', 'B&B'];

  protected secureAPIUrl = environment.SECURE_API_URL + '/lists.asp';
  lists: any;

  private _tour: Tour;
  countries = [];
  howReferred = [];

  public parsedListSource = new Subject<any>();
  public parsedListAnnounce = this.parsedListSource.asObservable();

  get tour(): Tour {
    return this._tour;
  }

  set tour(value: Tour) {
    this._tour = value;
    this.setTourAvailability(this._tour.available);
  }

  constructor(private http: Http) {
    this.getLists();
  }

  getLists() {
    this.http.get(this.secureAPIUrl)
      .map(response => response.json())
      .subscribe(
        data => this.lists = data,
        err => this.logError(err),
        () => this.sendData(this.lists)
      );
  }

  logError(error: any) {
    console.log('List API Call Error ' + error);
  }

  sendData(lists: any) {
    console.log('List API Call completed OK');
    this.parsedListSource.next(lists);
  }

  setTourAvailability(maxValue: number) {
    this.tourAvailability = [];
    this.roomCount = [];
    for (let a = 0; a <= maxValue; a++) {
      this.tourAvailability.push(a.toString(10));
      if (a !== 0 && a < 6) {
        this.roomCount.push(a.toString(10));
      }
    }
  }
}
