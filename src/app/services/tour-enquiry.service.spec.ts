import { TestBed, inject } from '@angular/core/testing';

import { TourEnquiryService } from './tour-enquiry.service';

describe('TourEnquiryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TourEnquiryService]
    });
  });

  it('should be created', inject([TourEnquiryService], (service: TourEnquiryService) => {
    expect(service).toBeTruthy();
  }));
});
