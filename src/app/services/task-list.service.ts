import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Task} from '../models/utilities/task';

@Injectable()
export class TaskListService {

  dummyCardCount = 10;

  dummy = true;
  tasks: Task[] = [];

  taskListSource = new Subject<Task[]>();
  public taskListAnnoounce = this.taskListSource.asObservable();

  constructor() {

  }

  getTasks() {
    if (this.dummy) {
      this.sendDummyTasks();
    }
  }

  sendDummyTasks() {
    for (let t = 0; t < 10; t++) {
      const task = new Task();
      task.description = 'Task ' + t.toString();
      if (t === 4) {
        task.priority = 1;
      }
      this.tasks.push(task);
    }
    console.log('Sending Tasks amount : ' + this.tasks.length);
    this.taskListSource.next(this.tasks);
  }
}
