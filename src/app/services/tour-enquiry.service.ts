import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class TourEnquiryService {

  private _runningTotal = 0;
  public runningTotalSource = new Subject<number>();
  public runningTotalAnnounce= this.runningTotalSource.asObservable();

  private _runningPaxTotal = 0;
  public runningPaxTotalSource = new Subject<number>();
  public runningPaxTotalAnnounce= this.runningPaxTotalSource.asObservable();

  get runningTotal(): number {
    return this._runningTotal;
  }

  set runningTotal(value: number) {
    this._runningTotal = value;
    this.runningTotalSource.next(value);
  }

  get runningPaxTotal(): number {
    return this._runningPaxTotal;
  }

  set runningPaxTotal(value: number) {
    this._runningPaxTotal = value;
    this.runningPaxTotalSource.next(value);
  }

  constructor() { }

  /** Tour Enquiries format info **
   *
   */
}
