
import {Headers} from '@angular/http';
import {HttpHeaders} from '@angular/common/http';

export class HeaderConfig  {


	public loggedIn: Boolean = false;

	constructor() {
	}

	createDefaultHeader() {
		const headers: Headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('rab_username', 'u8885734572385230572@rabbies.com');
		headers.append('rab_x2', '8885734572385230572');
		headers.append('rab_mobile', 'false');
		headers.append('rab_session', 'NULL');

		return headers;
	}

	createNewDefaultHeader() {
		const headers: HttpHeaders = new HttpHeaders();
		headers.append('Content-Type', 'application/json');
		headers.append('rab_username', 'u8885734572385230572@rabbies.com');
		headers.append('rab_x2', '8885734572385230572');
		headers.append('rab_mobile', 'false');
		headers.append('rab_session', 'NULL');

		return headers;
	}
}
