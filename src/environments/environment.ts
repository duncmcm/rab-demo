// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,

  SECURE_API_URL: 'https://secure.rabbies.com/api/rabbies/endpoints',
  // API_URL: 'http://booking-api-svc:31011/rest/api'
  // API_URL: 'http://192.168.4.28:31011/rest/api'
  API_URL: 'http://192.168.3.156/rest/api'

};
