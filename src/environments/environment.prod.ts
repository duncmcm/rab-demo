export const environment = {
  production: true,

  SECURE_API_URL: 'https://secure.rabbies.com/api/rabbies/endpoints',
  // API_URL: 'http://booking-api-svc:31011/rest/api'
  API_URL: 'http://192.168.4.28:31011/rest/api'

};
